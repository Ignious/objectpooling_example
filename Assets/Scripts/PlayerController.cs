﻿using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController SharedInstance; //define singleton of Player Controller

    public float moveSpeed, bulletSpeed, bulletDelay;
    bool canShoot = true;

    GameObject Player;
    Vector3 Movement;
    Rigidbody pBody;

    void Start()
    {
        Player = this.gameObject; //set player to the object containing this script

        if (Player.GetComponent<Rigidbody>() != null)
        {
            pBody = Player.GetComponent<Rigidbody>(); //set pBody to a reference of the players rigidbody
        }
    }

    void Update()
    {
        MovePlayer(); //once per frame run move player method
    }

    public void MovePlayer()
    {
        //create a new vector 3 which takes horizontal and vertical axis input and multiplies them by the movement speed
        Movement = new Vector3(Input.GetAxis("Horizontal") * moveSpeed, Input.GetAxis("Vertical") * moveSpeed, 0f);
        pBody.velocity = Movement; //apply the movement vector to the player rigidbodies velocity
    }

    public void FireBullet()
    {
        if (canShoot)
        {
            GameObject bulletClone = ObjectPooler.SharedInstance.GetObject(); //get a bullet from the object pooler
            if (bulletClone != null) //check if the bullet exists
            {
                bulletClone.transform.position = Player.transform.position; //set the bullet to the players position
                bulletClone.SetActive(true); //activate the bullet
            }

            //stop next bullet from being shot
            canShoot = false;
            StartCoroutine(FireDelay()); //create a short delay before next bullet can be shot.
        }
    }

    IEnumerator FireDelay()
    {
        yield return new WaitForSeconds(bulletDelay); //wait for the length of time designated by bullet delay
        canShoot = true; //enable can shoot
    }

    private void Awake()
    {
        SharedInstance = this; //set shared instance to this script
    }
}
