﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    public static ObjectPooler SharedInstance; //define singleton of objectPooler

    public List<GameObject> objectPool; //list to store the object pool
    public GameObject objectToPool; //public variable to store object to be pooled
    public int poolAmount; //public variable to determine the amount of the object to be pooled

    // Start is called before the first frame update
    void Start()
    {
        objectPool = new List<GameObject>();

        //instantiated object to pool the amount of times defined by the pool amount
        for (int i = 0; i < poolAmount; i++)
        {
            GameObject obj = (GameObject)Instantiate(objectToPool);
            obj.SetActive(false);
            objectPool.Add(obj);
        }
    }

    public GameObject GetObject()
    {
        //get the first object in the object pool which is currently inactive.
        for (int i = 0; i < objectPool.Count; i++)
        {
            if(!objectPool[i].activeInHierarchy)
            {
                return objectPool[i];
            }
        }
        //if none are inactive then return nothing.
        return null;
    }


    private void Awake()
    {
        SharedInstance = this; //set shared instance to this script
    }
}
