﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    CommandPattern FireWeapon;
    bool isActive = true;

    CommandPattern FireBullet = new FireWeapon(); //instantiating the firebullet command

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            HandleInput(); //while input is active run handle input once per frame
        }
    }

    public void HandleInput()
    {
        //if space is pressed execute the firebullet command.
        if (Input.GetKey(KeyCode.Space))
        {
            FireBullet.Execute();
        }
    }
}
