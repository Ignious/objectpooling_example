﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    Rigidbody bBody;
    public float moveSpeed;

    private void Start()
    {
        bBody = gameObject.GetComponent<Rigidbody>();
    }

    //when object is made active
    private void OnEnable()
    {
        StartCoroutine(Destroy());
    }

    private void Update()
    {
        bBody.velocity = new Vector3(1, 0, 0) * moveSpeed; //set constant move right based on speed variable
    }

    //waits for 2 seconds then sets the object to inactive
    IEnumerator Destroy()
    {
        yield return new WaitForSeconds(2f);
        gameObject.SetActive(false);
    }
}
