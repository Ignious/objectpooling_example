﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Command Pattern Class
public abstract class CommandPattern : MonoBehaviour
{
    public abstract void Execute();
}

//Child of CommandPattern used to call the fireBullet method
public class FireWeapon : CommandPattern
{
    public override void Execute()
    {
        PlayerController.SharedInstance.FireBullet();
    }
}
